<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Специалисты</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Head -->
        <?php include('inc/head.inc.php') ?><!-- -->

    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <div class="wrapper">
            <div class="content">

                <div class="container-fluid">

                    <h2>Специалисты</h2>

                    <div class="top-bar">
                        <ul>
                            <li>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="поиск по базе">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Искать</button>
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="btn btn-default" title="Экспортировать"><i class="fa fa-download" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>mail@email.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Доступ в систему</td>
                                        <td>
                                            <select class="form-control">
                                                <option value="открыт">открыт</option>
                                                <option value="закрыт">закрыт</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <ul class="pagination pull-right">
                            <li class="footable-page-arrow disabled"><a data-page="first" href="#first"><i class="fa fa-angle-double-left"></i></a></li>
                            <li class="footable-page-arrow disabled"><a data-page="prev" href="#prev"><i class="fa fa-angle-left"></i></a></li>
                            <li class="footable-page active"><a data-page="0" href="#">1</a></li>
                            <li class="footable-page"><a data-page="1" href="#">2</a></li>
                            <li class="footable-page-arrow"><a data-page="next" href="#next"><i class="fa fa-angle-right"></i></a>
                            </li><li class="footable-page-arrow"><a data-page="last" href="#last"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>

                    <div class="text-center">
                        <a data-src="#user-add" href="#" class="btn btn-success  btn-modal" title="Добавить"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Добавить специалиста</a>
                    </div>
                </div>

                <!-- New user -->
                <div class="hide">
                    <div class="modal-box" id="user-add">
                        <div class="modal-title">Добавить специалиста</div>
                        <form class="form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="ФИО специалиста">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input3" placeholder="Телефон">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </div>

                            <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option value="-">Доступ в систему</option>
                                        <option value="открыт">открыт</option>
                                        <option value="закрыт">закрыт</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row text-center">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>  <!-- -->

            </div>
        </div>

        <!-- Script -->
        <?php include('inc/script.inc.php') ?><!-- -->

    </body>
</html>
