<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Карточка клиента</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Head -->
        <?php include('inc/head.inc.php') ?><!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <div class="wrapper">
            <div class="content">

                <div class="container-fluid">

                    <h2>Пациент<br/><span>Волкова Елена</span></h2>

                    <table class="table table-bordered table-striped">

                        <tr>
                            <td>Телефон</td>
                            <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                        </tr>
                        <tr>
                            <td>Метро</td>
                            <td>Пражская</td>
                        </tr>
                        <tr>
                            <td>Округ</td>
                            <td>ЮАО</td>
                        </tr>
                        <tr>
                            <td>Улица</td>
                            <td>Чертановская 24</td>
                        </tr>
                        <tr>
                            <td>пол пациента</td>
                            <td>муж</td>
                        </tr>
                        <tr>
                            <td>Диагноз</td>
                            <td>Cras enim sem, hendrerit quis mi id.</td>
                        </tr>
                        <tr>
                            <td>Стоимость</td>
                            <td>9800 руб.</td>
                        </tr>
                        <tr>
                            <td>Дата обращения</td>
                            <td>15.03.2017</td>
                        </tr>
                        <tr>
                            <td>Дата начала</td>
                            <td>22.03.2017</td>
                        </tr>
                        <tr>
                            <td>Дата окончания</td>
                            <td>16.04.2017</td>
                        </tr>
                        <tr>
                            <td>Кол-во процедур</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>Закрепленный специалист</td>
                            <td>Павлов Дмитрий</td>
                        </tr>
                    </table>
                    <div class="text-center">
                        <a data-src="#user-change" href="#" class="btn btn-success  btn-modal" title="Изменить">Изменить</a>
                    </div>

                    <!-- Edit form -->
                    <div class="hide">
                        <div class="modal-box" id="user-change">
                            <div class="modal-title">Пациент <span>Волкова Елена</span></div>
                            <form class="form">
                                <div class="form-group text-center">
                                    <div class="form-group-title">Статус заказа</div>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> выполнен
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="options" id="option2" autocomplete="off"> отложен
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" placeholder="комментарий к заказу" rows="4"></textarea>
                                </div>
                                <div class="row text-center">
                                    <button type="submit" class="btn btn-primary">изменить</button>
                                </div>
                            </form>
                        </div>
                    </div>  <!-- -->

                </div>

            </div>
        </div>

        <!-- Script -->
        <?php include('inc/script.inc.php') ?><!-- -->
    
    </body>
</html>
