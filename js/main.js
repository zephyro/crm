$(function() {
    var pull = $('.navbar-hideshow');
    var box = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        box.toggleClass('show-menu');
    });
});



$('.ibox-toggle').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.ibox');
    box.toggleClass('open');
});

//tabs


$('.tabs-nav a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    console.log(tab);
    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$(".btn-modal").fancybox({
    Infobar: false,
    buttons: false
});




$('.form-date').datetimepicker({
    lang:'ru',
    timepicker:false,
    format:'d/m/Y',
    formatDate:'Y/m/d',
    minDate:0,
    maxDate:'2027/12/03'
});

$('.form-time').datetimepicker({
    datepicker:false,
    format:'H:i',
    step:15,
    minTime:'08:00'
});



$('.plan-nav a').click(function(e) {
    e.preventDefault();

    var box = $(this).closest('.plan');
    var dir = $(this).attr("data-target");
    var week = box.find('.current');

    if (dir == 'prev') {

        if (week.prev().is('.plan-week')) {
            week.removeClass('current');
            week.prev().addClass('current');
        }
    }

    if (dir == 'next') {

        if (week.next().is('.plan-week')) {
            week.removeClass('current');
            week.next().addClass('current');
        }
    }
});



$('.plan-switch').click(function(e) {
    e.preventDefault();

    $(this).toggleClass('switch');
    var box = $(this).closest('.plan');
    box.toggleClass('switch');
});

