<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Экран специалиста</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Head -->
        <?php include('inc/head.inc.php') ?><!-- -->

    </head>
    
    <body>
    
        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <div class="wrapper">
            <div class="content">

                <div class="container-fluid">

                    <h2>Смолов Павел</h2>

                    <div class="ibox open">

                        <div class="ibox-content">

                            <div class="plan">

                                <div class="plan-head clearfix">

                                    <div class="plan-nav clearfix">
                                        <a href="#" class="plan-prev" data-target="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                        <a href="#" class="plan-next" data-target="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                    </div>

                                    <a href="#" class="plan-switch">
                                        <span class="plan-switch-week">неделя</span>
                                        <span class="plan-switch-list">список</span>
                                    </a>

                                </div>

                                <div class="plan-body">

                                    <div class="plan-week">
                                        <div class="plan-title">27.03 - 02.04</div>

                                        <div class="view-week">
                                            <table class="week-table">
                                                <thead class="week-head">
                                                <tr>
                                                    <th class="plan-info">Инфо</th>
                                                    <th>ПН<span>27.03</span></th>
                                                    <th>ВТ<span>28.03</span></th>
                                                    <th>СР<span>29.03</span></th>
                                                    <th>ЧТ<span>30.03</span></th>
                                                    <th>ПТ<span>31.03</span></th>
                                                    <th>СБ<span>01.04</span></th>
                                                    <th>ВС<span>02.04</span></th>
                                                </tr>
                                                </thead>
                                                <tbody class="week-content">

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Баумаская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="view-list">
                                            <table class="list-table">

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПН</span>
                                                        <span class="week-date">27.03</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ВТ</span>
                                                        <span class="week-date">28.03</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-v" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СР</span>
                                                        <span class="week-date">29.03</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПТ</span>
                                                        <span class="week-date">31.03</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СБ</span>
                                                        <span class="week-date">01.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>

                                    </div>
                                    <div class="plan-week current">
                                        <div class="plan-title">03.04 - 09.04</div>

                                        <div class="view-week">
                                            <table class="week-table">
                                                <thead class="week-head">
                                                <tr>
                                                    <th class="plan-info">Инфо</th>
                                                    <th>ПН<span>03.04</span></th>
                                                    <th>ВТ<span>04.04</span></th>
                                                    <th>СР<span>05.04</span></th>
                                                    <th>ЧТ<span>06.04</span></th>
                                                    <th>ПТ<span>07.04</span></th>
                                                    <th>СБ<span>08.04</span></th>
                                                    <th>ВС<span>09.04</span></th>
                                                </tr>
                                                </thead>
                                                <tbody class="week-content">

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Баумаская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="view-list">
                                            <table class="list-table">

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПН</span>
                                                        <span class="week-date">03.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ВТ</span>
                                                        <span class="week-date">04.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СР</span>
                                                        <span class="week-date">05.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПТ</span>
                                                        <span class="week-date">07.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СБ</span>
                                                        <span class="week-date">08.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>

                                    </div>

                                    <div class="plan-week">
                                        <div class="plan-title">10.04 - 16.04</div>

                                        <div class="view-week">
                                            <table class="week-table">
                                                <thead class="week-head">
                                                <tr>
                                                    <th class="plan-info">Инфо</th>
                                                    <th>ПН<span>10.04</span></th>
                                                    <th>ВТ<span>11.04</span></th>
                                                    <th>СР<span>12.04</span></th>
                                                    <th>ЧТ<span>13.04</span></th>
                                                    <th>ПТ<span>14.04</span></th>
                                                    <th>СБ<span>15.04</span></th>
                                                    <th>ВС<span>16.04</span></th>
                                                </tr>
                                                </thead>
                                                <tbody class="week-content">

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Баумаская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th class="plan-info">
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal" title="">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </th>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-success" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-cancel" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-new" href="#" class="btn-modal plan-new" title=""></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="view-list">
                                            <table class="list-table">

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПН</span>
                                                        <span class="week-date">10.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ВТ</span>
                                                        <span class="week-date">11.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СР</span>
                                                        <span class="week-date">12.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="14:00">14:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-info" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-info" href="#" class="btn-modal plan-status plan-cancel"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">ПТ</span>
                                                        <span class="week-date">14.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Беляево</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Смолов Анатолий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>

                                                <tr class="list-head">
                                                    <th colspan="3">
                                                        <span class="week-day">СБ</span>
                                                        <span class="week-date">15.04</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="12:00">12:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Бауманская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Павлов Дмитрий</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal" title="16:00">16:00</a>
                                                    </td>
                                                    <td>
                                                        <a data-src="#user-change" href="#" class="btn-modal plan-status plan-success"></a>
                                                    </td>
                                                    <td>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">м Шаболовская</a></span>
                                                        <span><a data-src="#user-info" href="#" class="btn-modal">Иванова Елена</a></span>
                                                        <span><a href="tel:+79275000885">+7(927) 500-08-85</a></span>
                                                    </td>
                                                </tr>


                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

        <!-- Client Info -->
        <div class="hide">
            <div class="modal-box" id="user-info">
                <div class="modal-title">Заказчик <span>Волкова Елена</span></div>
                <table class="table table-bordered table-striped">

                    <tr>
                        <td>Телефон</td>
                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                    </tr>
                    <tr>
                        <td>Метро</td>
                        <td>Пражская</td>
                    </tr>
                    <tr>
                        <td>Округ</td>
                        <td>ЮАО</td>
                    </tr>
                    <tr>
                        <td>Улица</td>
                        <td>Чертановская 24</td>
                    </tr>
                    <tr>
                        <td>пол пациента</td>
                        <td>муж</td>
                    </tr>
                    <tr>
                        <td>Диагноз</td>
                        <td>Cras enim sem, hendrerit quis mi id.</td>
                    </tr>
                    <tr>
                        <td>Стоимость</td>
                        <td>9800 руб.</td>
                    </tr>
                    <tr>
                        <td>Дата обращения</td>
                        <td>15.03.2017</td>
                    </tr>
                    <tr>
                        <td>Дата начала</td>
                        <td>22.03.2017</td>
                    </tr>
                    <tr>
                        <td>Дата окончания</td>
                        <td>16.04.2017</td>
                    </tr>
                    <tr>
                        <td>Кол-во процедур</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Комментарий</td>
                        <td>Cras enim sem, hendrerit quis mi id. Cras enim sem, hendrerit quis mi id.</td>
                    </tr>
                </table>

                <div class="text-center">
                    <button data-fancybox-close class="btn btn-primary">закрыть</button>
                </div>
            </div>
        </div>  <!-- -->

        <!-- Edit form -->
        <div class="hide">
            <div class="modal-box modal-box-sm" id="user-change">
                <div class="modal-title">Заказчик <span>Волкова Елена</span></div>
                <form class="form">
                    <div class="form-group text-center">
                        <div class="form-group-title">Статус заказа</div>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="options" id="option1" autocomplete="off" checked> выполнен
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="options" id="option2" autocomplete="off"> отменен
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group-title">Время заказа</div>
                        <input type="text" class="form-control text-center form-sm" name="input4" placeholder="__:__">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="комментарий к заказу" rows="4"></textarea>
                    </div>
                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>  <!-- -->

        <!-- New date -->
        <div class="hide">
            <div class="modal-box modal-box-sm" id="user-new">
                <div class="modal-title">Заказчик <span>Волкова Елена</span></div>
                <form class="form">
                    <div class="form-group">
                        <div class="form-group-title">Время заказа</div>
                        <input type="text" class="form-control text-center form-sm" name="input4" placeholder="__:__">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="комментарий к заказу" rows="4"></textarea>
                    </div>
                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>  <!-- -->

        <!-- Script -->
        <?php include('inc/script.inc.php') ?><!-- -->
    
    </body>
</html>
