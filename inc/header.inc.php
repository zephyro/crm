<header class="header">
    <div class="header-link navbar-hideshow" data-target="body">
        <i class="fa fa-bars nav-open" aria-hidden="true"></i>
        <i class="fa fa-close nav-close" aria-hidden="true"></i>
    </div>
    <div class="small-logo">CRM</div>
</header>


<nav class="menu">
    <div class="menu-user">Администратор</div>
    <ul>
        <li>
            <a href="#">Общее расписание</a>
        </li>
        <li>
            <a href="#">Добавить заказ</a>
        </li>
        <li>
            <a href="#">Клиентская база</a>
        </li>
        <li>
            <a href="#">Добавить специалиста</a>
        </li>
        <li>
            <a href="#">Настройки системы</a>
        </li>
        <li>
            <a href="#">Выход</a>
        </li>
    </ul>
</nav>