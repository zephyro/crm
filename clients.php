<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Список клиентов</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Head -->
        <?php include('inc/head.inc.php') ?><!-- -->

    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <div class="wrapper">
            <div class="content">

                <div class="container-fluid clearfix">

                    <h2>Клиенты</h2>


                    <div class="top-bar">
                        <ul>
                            <li>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="поиск по базе">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Искать</button>
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="btn btn-default" title="Экспортировать"><i class="fa fa-download" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>


                    <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Смолов Юрий</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Иванова Елена/td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>жен</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Попова Юлия/td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>жен</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Смирнов Юрий</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Агапов Дмитрий</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Трунов Павел</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Бондаренко Михаил</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Иыванова Таьяна</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>жен</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Павлов Дмитрий</span>
                                            <div class="ibox-tools">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide ibox-toggle">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Бондаренко Павел</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="ibox ibox-white clients">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">
                                            <span class="clients-name ibox-toggle">Смолова Елена</span>
                                            <div class="ibox-tools ibox-toggle">
                                                <a class="manage">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <a class="showhide">
                                                    <i class="fa fa-chevron-up"></i>
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Телефон</td>
                                        <td><a href="tel:+7(902) 555-555-55">+7(902) 555-555-55</a></td>
                                    </tr>
                                    <tr>
                                        <td>Метро</td>
                                        <td>Пражская</td>
                                    </tr>
                                    <tr>
                                        <td>Округ</td>
                                        <td>ЮАО</td>
                                    </tr>
                                    <tr>
                                        <td>Улица</td>
                                        <td>Чертановская 24</td>
                                    </tr>
                                    <tr>
                                        <td>ФИО пациента</td>
                                        <td>Смолов Юрий</td>
                                    </tr>
                                    <tr>
                                        <td>пол пациента</td>
                                        <td>муж</td>
                                    </tr>
                                    <tr>
                                        <td>Диагноз</td>
                                        <td>Cras enim sem, hendrerit quis mi id.</td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td>9800 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Дата обращения</td>
                                        <td>15.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата начала</td>
                                        <td>22.03.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Дата окончания</td>
                                        <td>16.04.2017</td>
                                    </tr>
                                    <tr>
                                        <td>Кол-во процедур</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Ответсвенный специалист</td>
                                        <td>Иванов Петр</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <ul class="pagination pull-right">
                            <li class="footable-page-arrow disabled"><a data-page="first" href="#first"><i class="fa fa-angle-double-left"></i></a></li>
                            <li class="footable-page-arrow disabled"><a data-page="prev" href="#prev"><i class="fa fa-angle-left"></i></a></li>
                            <li class="footable-page active"><a data-page="0" href="#">1</a></li>
                            <li class="footable-page"><a data-page="1" href="#">2</a></li>
                            <li class="footable-page-arrow"><a data-page="next" href="#next"><i class="fa fa-angle-right"></i></a>
                            </li><li class="footable-page-arrow"><a data-page="last" href="#last"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Script -->
        <?php include('inc/script.inc.php') ?><!-- -->

    </body>
</html>
