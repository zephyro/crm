<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Новый заказ</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Head -->
        <?php include('inc/head.inc.php') ?><!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <div class="wrapper">
            <div class="content">

                <div class="container-fluid">

                    <h2>Новый заказ</h2>

                    <form class="form">

                        <div class="box">
                            <h3>Карточка заказчика</h3>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input1" placeholder="ФИО заказчика">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input2" placeholder="Статус (родитель, дедушка, бабушка)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input3" placeholder="Телефон 1">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input4" placeholder="Телефон 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input5" placeholder="Эл. почта">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Метро">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Адрес 1">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Адрес 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Город">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Округ">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="input6" placeholder="Примечание" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="box">
                            <h3>Карточка пациента</h3>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input1" placeholder="ФИО пациента">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input2" placeholder="Дата рождения">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input3" placeholder="Пол">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input4" placeholder="Диагноз">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input6" placeholder="Стоимость">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input5" placeholder="Дата обращения">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input5" placeholder="Дата начала">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="input5" placeholder="Дата окончания">
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="input6" placeholder="Количество процедур">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="-">Ответсвенный специалист</option>
                                    <option value="Володин Сергей">Володин Сергей</option>
                                    <option value="Павлов Дмитрий">Павлов Дмитрий</option>
                                    <option value="Матросова Оксана">Матросова Оксан</option>
                                </select>
                            </div>
                            <div class="form-group clearfix">
                                <label class="label-left">Нужен ли стол?</label>
                                <div class="btn-group pull-right" data-toggle="buttons">
                                    <label class="btn btn-default active">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked> да
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="options" id="option2" autocomplete="off"> нет
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="input6" placeholder="Примечание" rows="3"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <a data-src="#repeat" href="#" class="btn-line btn-modal" title="назначить повторный курс">назначить повторный курс</a>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class=" btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> добавить пациента</button>
                        </div>
                    </form>

                    <!-- Повторный курс -->
                    <div class="hide">
                        <div class="modal-box" id="repeat">
                            <div class="modal-title">Повторный курс</div>
                            <form class="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input1" placeholder="ФИО пациента">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input2" placeholder="Дата рождения">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input3" placeholder="Пол">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input4" placeholder="Диагноз">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input6" placeholder="Стоимость">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input5" placeholder="Дата обращения">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input5" placeholder="Дата начала">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input5" placeholder="Дата окончания">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="input6" placeholder="Количество процедур">
                                </div>
                                <div class="form-group">
                                    <select class="form-control">
                                        <option value="-">Ответсвенный специалист</option>
                                        <option value="Володин Сергей">Володин Сергей</option>
                                        <option value="Павлов Дмитрий">Павлов Дмитрий</option>
                                        <option value="Матросова Оксана">Матросова Оксан</option>
                                    </select>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="label-left">Нужен ли стол?</label>
                                    <div class="btn-group pull-right" data-toggle="buttons">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options" autocomplete="off" checked> да
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="options"  autocomplete="off"> нет
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="input6" placeholder="Примечание" rows="3"></textarea>
                                </div>
                                <div class="row text-center">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Добавить</button>
                                </div>
                            </form>
                        </div>
                    </div>  <!-- -->

                </div>

            </div>
        </div>


        <!-- Script -->
        <?php include('inc/script.inc.php') ?><!-- -->

    </body>
</html>
